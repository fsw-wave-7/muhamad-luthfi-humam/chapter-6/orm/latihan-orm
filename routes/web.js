const { Router } = require('express')
const bodyParser = require('body-parser')

const AuthController = require('../controllers/web/AuthController')
const HomeController = require('../controllers/web/HomeController')

const web = Router()

web.use(bodyParser.json())
web.use(bodyParser.urlencoded({ extended: true }))

const authController = new AuthController
const homeController = new HomeController


// login
web.get('/', authController.login)
web.post('/', authController.doLogin)

// tabel
web.get('/tabel', homeController.tabel)
web.post('/tabel', homeController.tabel)

// adduser
web.get('/form', homeController.form)
web.post('/form', homeController.add)

web.get('/add', homeController.add)
web.post('/save-user', homeController.saveUser)
web.get('/tabel', homeController.tabel)

module.exports = web