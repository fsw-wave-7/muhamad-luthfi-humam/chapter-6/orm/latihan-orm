const { User } = require('../../models');
const { join } = require('path')
const bcrypt = require('bcrypt')

class HomeController {

    tabel = (req, res) => {
        User.findAll()
        .then(users => {
            res.render(join(__dirname, '../../views/tabel'), {
                users: users
            })
        })
    }

    form = (req, res) => {
        User.findAll()
        .then(users => {
            res.render(join(__dirname, '../../views/form'), {
                users: users
            })
        })
    }

    add = (req, res) => {
        res.render(join(__dirname, '../../views/form'), {
            users: users
        })
    }

    saveUser = async (req, res) => {
        const salt = await bcrypt.genSalt(10)
        console.log(req.body)
        User.create({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: await bcrypt.hash(req.body.password, salt)
        })
        .then(() => {
            res.redirect('/tabel')
        }).catch(err => {
            console.log(err)
        })
    }
}

module.exports = HomeController